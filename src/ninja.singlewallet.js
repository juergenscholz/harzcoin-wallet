(function (wallets, qrCode) {
	var single = wallets.singlewallet = {
		isOpen: function () {
			return (document.getElementById("singlewallet").className.indexOf("selected") != -1);
		},

		open: function () {
			if (document.getElementById("thaleraddress").innerHTML == "") {
				single.generateNewAddressAndKey();
			}
			document.getElementById("singlearea").style.display = "block";
		},

		close: function () {
			document.getElementById("singlearea").style.display = "none";
		},

		// generate harzcoin address and private key and update information in the HTML
		generateNewAddressAndKey: function () {
			try {
				var key = new Harzcoin.ECKey(false);
				key.setCompressed(true);
				var harzcoinAddress = key.getHarzcoinAddress();
				var privateKeyWif = key.getHarzcoinWalletImportFormat();
				document.getElementById("thaleraddress").innerHTML = harzcoinAddress;
				document.getElementById("thalerprivwif").innerHTML = privateKeyWif;
				var keyValuePair = {
					"qrcode_public": harzcoinAddress,
					"qrcode_private": privateKeyWif
				};
				qrCode.showQrCode(keyValuePair, 4);
			}
			catch (e) {
				// browser does not have sufficient JavaScript support to generate a harzcoin address
				alert(e);
				document.getElementById("thaleraddress").innerHTML = "error";
				document.getElementById("thalerprivwif").innerHTML = "error";
				document.getElementById("qrcode_public").innerHTML = "";
				document.getElementById("qrcode_private").innerHTML = "";
			}
		}
	};
})(ninja.wallets, ninja.qrCode);
